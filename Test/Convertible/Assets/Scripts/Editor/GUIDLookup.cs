﻿using UnityEditor;
using UnityEngine;

public class GUIDLookup : EditorWindow
{
	[MenuItem("Tools/GUID Lookup")]
	public static void Open()
	{
		GetWindow<GUIDLookup>();
	}

	private Object m_selection;
	private string m_guid;
	private string m_fileId;

	private void OnGUI()
	{
		using (new GUILayout.HorizontalScope())
		{
			m_selection = EditorGUILayout.ObjectField(m_selection, typeof(Object), true);
			if (GUILayout.Button("Find", EditorStyles.miniButton, GUILayout.Width(50.0f)))
			{
				GetData(m_selection);
			}
		}

		EditorGUILayout.Space();

		using (new GUILayout.HorizontalScope())
		{
			EditorGUILayout.LabelField("Guid", GUILayout.Width(50.0f));
			EditorGUILayout.TextField(m_guid);
		}

		using (new GUILayout.HorizontalScope())
		{
			EditorGUILayout.LabelField("FileId", GUILayout.Width(50.0f));
			EditorGUILayout.TextField(m_fileId);
		}
	}

	private void GetData(Object obj)
	{
		string path = AssetDatabase.GetAssetPath(obj);
		m_guid = AssetDatabase.AssetPathToGUID(path).Replace("\n", "").Replace("\r", "");
		m_fileId = Unsupported.GetLocalIdentifierInFile(obj.GetInstanceID()).ToString();
	}
}