﻿using Library;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class MaterialColor : MonoBehaviour
{
	[SerializeField]
	private ColorData m_colors;

	[SerializeField]
	private int m_index;

	private void Awake()
	{
		Renderer renderer = GetComponent<Renderer>();
		foreach (Material material in renderer.materials)
		{
			material.color = m_colors.Colors[m_index];
		}
	}
}