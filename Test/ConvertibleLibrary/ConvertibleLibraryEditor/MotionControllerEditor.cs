﻿using UnityEditor;
using UnityEngine;

namespace Library
{
	[CustomEditor(typeof(MotionController))]
	public class MotionControllerEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			using (new GUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Run", EditorStyles.miniButtonLeft))
				{
					(serializedObject.targetObject as MotionController).Play();
				}
				if (GUILayout.Button("Pause", EditorStyles.miniButtonRight))
				{
					(serializedObject.targetObject as MotionController).Pause();
				}
			}
		}
	}
}