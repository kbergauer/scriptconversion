﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Library
{
	[CreateAssetMenu(fileName = "NewColorData")]
	public class ColorData : ScriptableObject
	{
		[SerializeField]
		private string m_name;

		[SerializeField]
		private List<Color> m_colors;

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public List<Color> Colors
		{
			get { return m_colors; }
			set { m_colors = value; }
		}
	}
}