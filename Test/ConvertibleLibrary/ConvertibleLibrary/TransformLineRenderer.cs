﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Library
{
	[RequireComponent(typeof(LineRenderer))]
	public class TransformLineRenderer : MonoBehaviour
	{
		private LineRenderer m_lineRenderer;

		private void Awake()
		{
			m_lineRenderer = GetComponent<LineRenderer>();
			Apply();
		}

		public void Apply()
		{
			List<Vector3> positions = new List<Vector3>();
			for (int i = 0; i < transform.childCount; i++)
			{
				positions.Add(transform.GetChild(i).position);
			}
			positions.Add(positions[0]);

			m_lineRenderer.positionCount = positions.Count;
			m_lineRenderer.SetPositions(positions.ToArray());
		}
	}
}