﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Library
{
	public class MotionController : MonoBehaviour
	{
		[SerializeField]
		private List<Transform> m_waypoints;

		[SerializeField]
		private int m_currentWaypoint;

		[SerializeField]
		private float m_speed = 1.0f;

		[SerializeField]
		private float m_threshold = 0.01f;

		private bool m_isRunning;

		public void Play()
		{
			m_isRunning = true;
			StartCoroutine(DelayedLog("Play"));
		}

		public void Pause()
		{
			m_isRunning = false;
			StartCoroutine(DelayedLog("Pause"));
		}

		private void Update()
		{
			if (!m_isRunning)
			{
				return;
			}

			Vector3 direction = m_waypoints[m_currentWaypoint].position - transform.position;
			if (direction.magnitude <= m_threshold)
			{
				transform.position = m_waypoints[m_currentWaypoint].position;
				m_currentWaypoint++;
				if (m_currentWaypoint >= m_waypoints.Count)
				{
					m_currentWaypoint = 0;
				}
				return;
			}

			transform.position += direction.normalized * m_speed * Time.deltaTime;
		}

		private IEnumerator DelayedLog(string message)
		{
			yield return null;
			Debug.Log(message);
		}
	}
}