﻿using ScriptConverter.Collection;
using ScriptConverter.Logging;
using System;
using System.IO;

namespace ScriptConverter
{
	/// <summary>
	/// Class providing the entry point for the application.
	/// </summary>
	public class Program
	{
		/// <summary>
		/// Entry point method for the application.
		/// </summary>
		/// <param name="args">The arguments sent to the application.</param>
		private static void Main(string[] args)
		{
			ConsoleLogger logger = new ConsoleLogger();
			
			try
			{
				Program program = new Program();
				program.LogResult(program.Execute(logger), logger);
			}
			catch (Exception ex)
			{
				logger.Error($"Exception occurred: {ex}");
			}

			Console.ReadLine();
		}

		/// <summary>
		/// Executes the application.
		/// </summary>
		/// <param name="logger">The logger to use.</param>
		public ConversionResult Execute(ILogger logger)
		{
			string path = GetUserInput("Enter base path: ", @"D:\Data\Projects\ScriptConversion\Test\Convertible\Assets\", logger);
			logger.Info($"Starting conversion of data at '{path}'\n");

			ConversionResult result = ConversionResult.Success;

			FileSet<FileEntry> oldScripts = GetScripts("Enter old script path: ", path, logger);
			if (oldScripts == null)
			{
				return ConversionResult.MissingOldScripts;
			}

			FileSet<FileEntry> newScripts = GetScripts("Enter new script path: ", path, logger);
			if (newScripts == null)
			{
				return ConversionResult.MissingNewScripts;
			}

			Converter converter = new Converter(logger);
			return converter.Execute(path, new string[] { ".unity", ".prefab", ".asset" }, oldScripts, newScripts);
		}

		/// <summary>
		/// Gets the base file path from the user, providing a base value of none was entered.
		/// </summary>
		/// <param name="logger">The logger to use for output.</param>
		/// <returns>The base file path to use.</returns>
		private string GetUserInput(string prompt, string fallback, ILogger logger)
		{
			logger.Write(ConsoleColor.White, prompt);
			string path = Console.ReadLine();
			if (string.IsNullOrEmpty(path))
			{
				path = fallback;
			}

			return path;
		}

		/// <summary>
		/// Gets script file information by prompting the user for file path and ending.
		/// </summary>
		/// <param name="prompt">The prompt to display to the user.</param>
		/// <param name="logger">The logger to use for output.</param>
		/// <returns></returns>
		private FileSet<FileEntry> GetScripts(string prompt, string basePath, ILogger logger)
		{
			string relativePath = GetUserInput(prompt, "", logger);
			string fileType = GetUserInput("Enter file type (dll/cs): ", "cs", logger);

			switch (fileType)
			{
				case "dll":
					return new DllFileCollector(logger).Collect(Path.Combine(basePath, relativePath), ".dll");

				case "cs":
					return new MetaFileCollector(logger).Collect(Path.Combine(basePath, relativePath), ".cs.meta");
			}

			return null;
		}

		/// <summary>
		/// Logs the specified result using the provided logger.
		/// </summary>
		/// <param name="logger">The logger to use.</param>
		public void LogResult(ConversionResult result, ILogger logger)
		{
			if (result == ConversionResult.Success)
			{
				logger.Success("Conversion succeeded");
			}
			else
			{
				logger.Error($"Conversion failed with error: {result}");
			}
		}
	}
}
