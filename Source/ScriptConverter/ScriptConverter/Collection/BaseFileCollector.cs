﻿using ScriptConverter.Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace ScriptConverter.Collection
{
	/// <summary>
	/// Base class for file collectors providing common functionality.
	/// </summary>
	/// <typeparam name="T">The type to represent a single file in the created <see cref="FileSet{T}"/>.</typeparam>
	public abstract class BaseFileCollector<T> : IFileCollector<T>
	{
		/// <summary>
		/// The logger used to output information and errors.
		/// </summary>
		protected ILogger m_logger;

		/// <summary>
		/// Constructor caching the logger to utilize.
		/// </summary>
		/// <param name="logger"></param>
		public BaseFileCollector(ILogger logger)
		{
			m_logger = logger;
		}

		/// <summary>
		/// Collects all files matching the filter from the specified path.
		/// </summary>
		/// <param name="path">The path to collect files from.</param>
		/// <param name="filter">The file endings to filter for.</param>
		/// <returns>A <see cref="FileSet{T}"/> containing all matching files of the path.</returns>
		public FileSet<T> Collect(string path, params string[] filter)
		{
			try
			{
				string[] allFiles = GetFilesAtPath(path);
				if (allFiles == null || allFiles.Length < 1)
				{
					m_logger.Warn($"No files found at path '{path}'!");
					return null;
				}

				string[] filteredFiles = GetFilteredFiles(allFiles, filter);
				if (filteredFiles == null || filteredFiles.Length < 1)
				{
					m_logger.Warn($"No files found at path '{path}' with endings '{string.Join("/", filter)}'!");
					return null;
				}

				return CreateFileSet(filteredFiles);
			}
			catch (Exception ex)
			{
				m_logger.Error($"Exception occurred while collecting files: {ex}");
				return new FileSet<T>();
			}
		}

		/// <summary>
		/// Gets all files at the specified path, including sub-directories.
		/// </summary>
		/// <param name="path">The path to collect all files from.</param>
		/// <returns>An array containing all found file paths.</returns>
		protected string[] GetFilesAtPath(string path)
		{
			List<string> files = new List<string>();
			files.AddRange(Directory.GetFiles(path));

			string[] directories = Directory.GetDirectories(path);
			for (int i = 0; i < directories.Length; i++)
			{
				files.AddRange(GetFilesAtPath(directories[i]));
			}

			return files.ToArray();
		}

		/// <summary>
		/// Filters the provided files by the specified file ending filter.
		/// </summary>
		/// <param name="files">The files to filter.</param>
		/// <param name="filter">The file ending filter to apply.</param>
		/// <returns>The files matching the filter.</returns>
		protected string[] GetFilteredFiles(string[] files, string[] filter)
		{
			if (filter == null || filter.Length < 1)
			{
				return files;
			}

			List<string> filteredFiles = new List<string>();
			for (int i = 0; i < files.Length; i++)
			{
				for (int j = 0; j < filter.Length; j++)
				{
					if (!files[i].EndsWith(filter[j]))
					{
						continue;
					}
					filteredFiles.Add(files[i]);
				}
			}

			return filteredFiles.ToArray();
		}

		/// <summary>
		/// Empty implementation overridden by derivd clases to create a new <see cref="FileSet{T}"/> from the specified file paths.
		/// </summary>
		/// <param name="files">The files to save in the set.</param>
		/// <returns>A new <see cref="FileSet{T}"/> containing the specified files.</returns>
		protected abstract FileSet<T> CreateFileSet(string[] files);
		
		/// <summary>
		/// Gets the name of a file from its path.
		/// </summary>
		/// <param name="file">The file to get the name of.</param>
		/// <returns>A file's name.</returns>
		protected string GetFileNameFromPath(string file)
		{
			int slash = file.LastIndexOf('\\');
			int dot = file.IndexOf('.');

			return file.Substring(slash + 1, dot - slash - 1);
		}

		/// <summary>
		/// Gets the guid from a Unity meta file's content.
		/// </summary>
		/// <param name="file">The meta file to search for a guid.</param>
		/// <returns><c>string.Empty</c> if no guid could be found in the file's content, otherwise the found guid.</returns>
		protected string GetGuidFromFile(string file)
		{
			string[] content = File.ReadAllLines(file);
			for (int i = 0; i < content.Length; i++)
			{
				if (!content[i].StartsWith("guid: "))
				{
					continue;
				}

				return content[i].Replace("guid: ", "");
			}

			return string.Empty;
		}
	}
}
