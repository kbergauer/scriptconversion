﻿using ScriptConverter.Logging;

namespace ScriptConverter.Collection
{
	/// <summary>
	/// Class used to collect Unity files and their important data in a <see cref="FileSet{T}"/> of <see cref="string"/>.
	/// </summary>
	public class UnityFileCollector : BaseFileCollector<string>
	{
		/// <summary>
		/// Constructor initializing the base class.
		/// </summary>
		/// <param name="logger">The logger to use.</param>
		public UnityFileCollector(ILogger logger) : base(logger)
		{
		}

		/// <summary>
		/// Collects all files matching the filter from the specified path.
		/// </summary>
		/// <param name="path">The path to collect files from.</param>
		/// <param name="filter">The file endings to filter for.</param>
		/// <returns>A <see cref="FileSet{T}"/> containing all matching files of the path.</returns>
		protected override FileSet<string> CreateFileSet(string[] files)
		{
			FileSet<string> set = new FileSet<string>();
			for (int i = 0; i < files.Length; i++)
			{
				set.Entries.Add(
					files[i],
					GetFileNameFromPath(files[i])
				);
			}

			return set;
		}
	}
}
