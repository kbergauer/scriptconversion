﻿namespace ScriptConverter.Collection
{
	/// <summary>
	/// Interface for classes collecting files from the system.
	/// </summary>
	/// <typeparam name="T">The type representing a single file in the resulting <see cref="FileSet{T}"/>.</typeparam>
	public interface IFileCollector<T>
	{
		/// <summary>
		/// Collects all files matching the filter from the specified path.
		/// </summary>
		/// <param name="path">The path to collect files from.</param>
		/// <param name="filter">The file endings to filter for.</param>
		/// <returns>A <see cref="FileSet{T}"/> containing all matching files of the path.</returns>
		FileSet<T> Collect(string path, params string[] filter);
	}
}
