﻿namespace ScriptConverter.Collection
{
	/// <summary>
	/// Class representing a file in a <see cref="FileSet{T}"/>.
	/// </summary>
	public class FileEntry
	{
		/// <summary>
		/// The display name of the file.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// The guid of the file.
		/// </summary>
		public string Guid { get; set; }

		/// <summary>
		/// The local identifier of the file.
		/// </summary>
		public string FileId { get; set; }
		
		/// <summary>
		/// Override creating a string from all important properties.
		/// </summary>
		/// <returns>A string containing the <see cref="Name"/>, <see cref="Guid"/> and <see cref="FileId"/> of the entry.</returns>
		public override string ToString()
		{
			return $"[{Name}, {Guid}, {FileId}]";
		}

		/// <summary>
		/// Gets the representation of the entry in a Unity file.
		/// </summary>
		/// <returns>The representation of the file.</returns>
		public string GetFileElement()
		{
			return $"fileID: {FileId}, guid: {Guid}";
		}
	}
}
