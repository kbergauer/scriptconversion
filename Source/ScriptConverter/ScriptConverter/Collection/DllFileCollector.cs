﻿using ScriptConverter.Logging;
using ScriptConverter.Utilities;
using System;
using System.IO;
using System.Reflection;

namespace ScriptConverter.Collection
{
	/// <summary>
	/// Class used to collect classes from a dll files and their important data in a <see cref="FileSet{T}"/> of <see cref="FileEntry"/>.
	/// </summary>
	public class DllFileCollector : BaseFileCollector<FileEntry>
	{
		/// <summary>
		/// Constructor initializing the base class.
		/// </summary>
		/// <param name="logger">The logger to use.</param>
		public DllFileCollector(ILogger logger) : base(logger)
		{
		}

		/// <summary>
		/// Collects all files matching the filter from the specified path and enters their exposed types in <see cref="FileEntry"/> instances.
		/// </summary>
		/// <param name="path">The path to collect files from.</param>
		/// <param name="filter">The file endings to filter for.</param>
		/// <returns>A <see cref="FileSet{T}"/> containing the exposed types of all matching files of the path.</returns>
		protected override FileSet<FileEntry> CreateFileSet(string[] files)
		{
			FileSet<FileEntry> set = new FileSet<FileEntry>();
			for (int i = 0; i < files.Length; i++)
			{
				string guid = GetGuidForFile(files[i]);
				if (string.IsNullOrEmpty(guid))
				{
					continue;
				}

				Type[] types = GetTypesFromFile(files[i]);
				for (int j = 0; j < types.Length; j++)
				{
					set.Entries.Add(
						types[j].FullName,
						new FileEntry
						{
							Name = types[j].Name,
							Guid = guid,
							FileId = FileIdUtility.Compute(types[j]).ToString()
						}
					);
				}
			}

			return set;
		}

		/// <summary>
		/// Gets the guid of the specified file from its Unity meta file.
		/// </summary>
		/// <param name="file">The file to get the guid for.</param>
		/// <returns><c>null</c> if no meta file could be found, <c>string.Empty</c> if no guid could be parsed from the content, otherwise the parsed guid.</returns>
		private string GetGuidForFile(string file)
		{
			file += ".meta";
			if (!File.Exists(file))
			{
				return null;
			}

			return GetGuidFromFile(file);
		}

		/// <summary>
		/// Gets the exposed types of the dll loaded from the provided file path.
		/// </summary>
		/// <param name="file">The file path to load the assembly from.</param>
		/// <returns>An array of the exposed types.</returns>
		private Type[] GetTypesFromFile(string file)
		{
			Assembly assembly = Assembly.LoadFile(file);
			return assembly.GetExportedTypes();
		}
	}
}
