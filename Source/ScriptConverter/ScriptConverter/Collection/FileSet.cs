﻿using System.Collections.Generic;

namespace ScriptConverter.Collection
{
	/// <summary>
	/// Class containing a dictionary of files and their necessary data.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class FileSet<T>
	{
		/// <summary>
		/// The dictionary mapping file paths to their specific necessary data.
		/// </summary>
		public Dictionary<string, T> Entries { get; set; }

		/// <summary>
		/// Initializes the dictionary.
		/// </summary>
		public FileSet()
		{
			Entries = new Dictionary<string, T>();
		}
		
		/// <summary>
		/// Override creating a string from all current entries of the set.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return $"FileSet({Entries.Count})\n{string.Join("\n", Entries.Values)}";
		}
	}
}
