﻿using ScriptConverter.Logging;

namespace ScriptConverter.Collection
{
	/// <summary>
	/// Class used to collect meta files and their important data in a <see cref="FileSet{T}"/> of <see cref="FileEntry"/>.
	/// </summary>
	public class MetaFileCollector : BaseFileCollector<FileEntry>
	{
		/// <summary>
		/// Constructor initializing the base class.
		/// </summary>
		/// <param name="logger">The logger to use.</param>
		public MetaFileCollector(ILogger logger) : base(logger)
		{
		}

		/// <summary>
		/// Collects all files matching the filter from the specified path, filling their <see cref="FileEntry"/> with meta file data.
		/// </summary>
		/// <param name="path">The path to collect files from.</param>
		/// <param name="filter">The file endings to filter for.</param>
		/// <returns>A <see cref="FileSet{T}"/> containing all matching files of the path.</returns>
		protected override FileSet<FileEntry> CreateFileSet(string[] files)
		{
			FileSet<FileEntry> set = new FileSet<FileEntry>();
			for (int i = 0; i < files.Length; i++)
			{
				string guid = GetGuidFromFile(files[i]);
				if (string.IsNullOrEmpty(guid))
				{
					continue;
				}

				set.Entries.Add(
					files[i],
					new FileEntry
					{
						Name = GetFileNameFromPath(files[i]),
						Guid = guid,
						FileId = "11500000"
					}
				);
			}

			return set;
		}
	}
}
