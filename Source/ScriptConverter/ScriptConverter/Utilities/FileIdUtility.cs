﻿using System;
using System.Security.Cryptography;

namespace ScriptConverter.Utilities
{
	public class FileIdUtility
	{
		public static int Compute(Type type)
		{
			string toBeHashed = "s\0\0\0" + type.Namespace + type.Name;

			using (HashAlgorithm hash = new MD4())
			{
				byte[] hashed = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(toBeHashed));

				int result = 0;

				for (int i = 3; i >= 0; --i)
				{
					result <<= 8;
					result |= hashed[i];
				}

				return result;
			}
		}
	}
}
