﻿using ScriptConverter.Collection;
using ScriptConverter.Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace ScriptConverter
{
	/// <summary>
	/// Class handling the conversion of Unity files.
	/// </summary>
	public class Converter
	{
		/// <summary>
		/// The logger used for general output.
		/// </summary>
		private ILogger m_logger;

		/// <summary>
		/// The logger used for output during the conversion process.
		/// </summary>
		private ILogger m_conversionLogger;

		/// <summary>
		/// Constructor caching the logger for later reference and creating a conversion logger.
		/// </summary>
		/// <param name="logger">The generally used logger.</param>
		public Converter(ILogger logger)
		{
			m_logger = logger;
			m_conversionLogger = new FileLogger("output.txt");
		}

		/// <summary>
		/// Executes the conversion of the specified files.
		/// </summary>
		/// <param name="path">The path to search for convertible files.</param>
		/// <param name="targets">The targeted file endings.</param>
		/// <param name="oldScripts">The old scripts to convert the files from.</param>
		/// <param name="newScripts">The new scripts to convert the files to.</param>
		/// <returns>The result of the conversion.</returns>
		public ConversionResult Execute(string path, string[] targets, FileSet<FileEntry> oldScripts, FileSet<FileEntry> newScripts)
		{
			if (targets == null || targets.Length < 1)
			{
				return ConversionResult.MissingTargets;
			}

			if (oldScripts == null || oldScripts.Entries == null || oldScripts.Entries.Count < 1)
			{
				return ConversionResult.MissingOldScripts;
			}
			m_logger.Info($"Old Scripts: {oldScripts}\n");

			if (newScripts == null || newScripts.Entries == null || newScripts.Entries.Count < 1)
			{
				return ConversionResult.MissingNewScripts;
			}
			m_logger.Info($"New Scripts: {newScripts}\n");

			UnityFileCollector assetCollector = new UnityFileCollector(m_logger);
			List<FileSet<string>> files = new List<FileSet<string>>();
			int fileCount = 0;
			for (int i = 0; i < targets.Length; i++)
			{
				files.Add(assetCollector.Collect(path, targets[i]));
				m_logger.Info($"{targets[i]}: {files[i]}\n");

				if (files[i] != null && files[i].Entries != null)
				{
					fileCount += files[i].Entries.Count;
				}
			}

			Console.CursorVisible = false;
			m_logger.Write(ConsoleColor.White, "Conversion: ");
			ProgressLogger progressLogger = new ProgressLogger(fileCount, m_logger);
			
			ConversionResult result = ConversionResult.Success;
			for (int i = 0; i < files.Count; i++)
			{
				result |= Convert(files[i], oldScripts, newScripts, progressLogger);
			}

			progressLogger.SetProgess(100);
			m_logger.Write(ConsoleColor.White, "\n");
			Console.CursorVisible = true;

			return result;
		}

		/// <summary>
		/// Converts the files in the provided <see cref="FileSet{T}"/> using the old and new scripts.
		/// </summary>
		/// <param name="files">The files to convert.</param>
		/// <param name="oldScripts">The old scripts to convert from.</param>
		/// <param name="newScripts">The new scripts to convert to.</param>
		/// <param name="progressLogger">The logger used to display the general conversion progress.</param>
		/// <returns>The result of the conversion.</returns>
		private ConversionResult Convert(FileSet<string> files, FileSet<FileEntry> oldScripts, FileSet<FileEntry> newScripts, ProgressLogger progressLogger)
		{
			if (files.Entries == null || files.Entries.Count < 1)
			{
				return ConversionResult.Success;
			}

			foreach (string file in files.Entries.Keys)
			{
				m_conversionLogger.Info($"Starting conversion of file '{file}'");

				List<string> content = new List<string>();
				using (StreamReader reader = new StreamReader(file))
				{
					string line;
					int index = 0;
					while ((line = reader.ReadLine()) != null)
					{
						content.Add(ConvertLine(++index, line, oldScripts, newScripts));
					}
				}

				File.WriteAllLines(file, content);

				m_conversionLogger.Info($"Finished conversion of file '{file}'\n");
				progressLogger.Advance();
			}

			return ConversionResult.Success;
		}

		/// <summary>
		/// Converts the line from old to new scripts by replacing their file element (file identifier and guid).
		/// </summary>
		/// <param name="lineIndex">The index of the line used for log output.</param>
		/// <param name="line">The line to convert.</param>
		/// <param name="oldScripts">The old scripts to convert from.</param>
		/// <param name="newScripts">The new scripts to convert to.</param>
		/// <returns></returns>
		private string ConvertLine(int lineIndex, string line, FileSet<FileEntry> oldScripts, FileSet<FileEntry> newScripts)
		{
			foreach (FileEntry oldScript in oldScripts.Entries.Values)
			{
				if (!line.Contains(oldScript.GetFileElement()))
				{
					continue;
				}

				FileEntry newScript = GetMatchingFileEntry(oldScript, newScripts);
				if (newScript == null)
				{
					continue;
				}
				
				line = line.Replace(oldScript.GetFileElement(), newScript.GetFileElement());
				m_conversionLogger.Info($"Replacing {oldScript} with {newScript} in line {lineIndex}");
			}

			return line;
		}

		/// <summary>
		/// Gets the entry from a <see cref="FileSet{T}"/> matching the provided entry's name.
		/// </summary>
		/// <param name="entry">The entry to get a match for.</param>
		/// <param name="possibleMatches">The file set to get a match from.</param>
		/// <returns></returns>
		private FileEntry GetMatchingFileEntry(FileEntry entry, FileSet<FileEntry> possibleMatches)
		{
			foreach (FileEntry possibleMatch in possibleMatches.Entries.Values)
			{
				if (!entry.Name.Equals(possibleMatch.Name))
				{
					continue;
				}

				return possibleMatch;
			}

			return null;
		}
	}
}
