﻿namespace ScriptConverter
{
	/// <summary>
	/// All possible results of a conversion attempt.
	/// </summary>
	public enum ConversionResult
	{
		/// <summary>
		/// Returned when the conversion was successful.
		/// </summary>
		Success,
		
		/// <summary>
		/// Returned when the conversion was not provided any targets.
		/// </summary>
		MissingTargets,

		/// <summary>
		/// Returned when the conversion was not provided any old scripts to convert from.
		/// </summary>
		MissingOldScripts,

		/// <summary>
		/// Returned when the conversion was not provided any new scripts to convert to.
		/// </summary>
		MissingNewScripts
	}
}
