﻿using System;
using System.IO;

namespace ScriptConverter.Logging
{
	/// <summary>
	/// Logger caching all provided messages, writing them to a file before destruction.
	/// </summary>
	public class FileLogger : ILogger
	{
		/// <summary>
		/// The path of the file to write to.
		/// </summary>
		private string m_filePath;

		/// <summary>
		/// The content collected so far to be written to a file when closing the logger.
		/// </summary>
		private string m_content;

		/// <summary>
		/// Constructor initializing the logger with the path of the file to write to.
		/// </summary>
		/// <param name="path">The path of the file to write into.</param>
		public FileLogger(string path)
		{
			m_filePath = path;
		}

		/// <summary>
		/// Destructor writing all collected messages into a file.
		/// </summary>
		~FileLogger()
		{
			File.WriteAllText(m_filePath, m_content);
		}

		/// <summary>
		/// Caches a debug message.
		/// Gets stripped out for release builds.
		/// </summary>
		/// <param name="message">The message to cache.</param>
		public void Debug(string message)
		{
#if DEBUG
			WriteLine(ConsoleColor.White, $"[D] {message}");
#endif
		}

		/// <summary>
		/// Caches an info message.
		/// </summary>
		/// <param name="message">The message to cache.</param>
		public void Info(string message)
		{
			WriteLine(ConsoleColor.White, $"[I] {message}");
		}

		/// <summary>
		/// Caches a warning message.
		/// </summary>
		/// <param name="message">The message to cache.</param>
		public void Warn(string message)
		{
			WriteLine(ConsoleColor.Yellow, $"[W] {message}");
		}

		/// <summary>
		/// Caches an error message.
		/// </summary>
		/// <param name="message">The message to cacehe.</param>
		public void Error(string message)
		{
			WriteLine(ConsoleColor.Red, $"[E] {message}");
		}

		/// <summary>
		/// Caches a success message.
		/// </summary>
		/// <param name="message">The message to cache.</param>
		public void Success(string message)
		{
			WriteLine(ConsoleColor.Green, $"[S] {message}");
		}

		/// <summary>
		/// Caches a message without leaving the current line.
		/// </summary>
		/// <param name="message">The message to cache.</param>
		public void Write(ConsoleColor color, string message)
		{
			m_content += message;
		}

		/// <summary>
		/// Caches a message leaving the current line afterwards.
		/// </summary>
		/// <param name="message">The message to cache.</param>
		public void WriteLine(ConsoleColor color, string message)
		{
			m_content += message + Environment.NewLine;
		}
	}
}
