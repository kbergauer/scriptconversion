﻿using System;

namespace ScriptConverter.Logging
{
	/// <summary>
	/// Logger writing messages to the default console.
	/// </summary>
	public class ConsoleLogger : ILogger
	{
		/// <summary>
		/// Logs a debug message to the console using <see cref="ConsoleColor.White"/> which gets stripped out for release builds.
		/// </summary>
		/// <param name="message">The message to log.</param>
		public void Debug(string message)
		{
#if DEBUG
			WriteLine(ConsoleColor.White, message);
#endif 
		}

		/// <summary>
		/// Logs an info message to the console using <see cref="ConsoleColor.White"/>.
		/// </summary>
		/// <param name="message">The message to log.</param>
		public void Info(string message)
		{
			WriteLine(ConsoleColor.White, message);
		}

		/// <summary>
		/// Logs a warning message to the console using <see cref="ConsoleColor.Yellow"/>.
		/// </summary>
		/// <param name="message">The message to log.</param>
		public void Warn(string message)
		{
			WriteLine(ConsoleColor.Yellow, message);
		}

		/// <summary>
		/// Logs an error message to the console using <see cref="ConsoleColor.Red"/>.
		/// </summary>
		/// <param name="message">The message to log.</param>
		public void Error(string message)
		{
			WriteLine(ConsoleColor.Red, message);
		}

		/// <summary>
		/// Logs a success message to the console using <see cref="ConsoleColor.Green"/>.
		/// </summary>
		/// <param name="message">The message to log.</param>
		public void Success(string message)
		{
			WriteLine(ConsoleColor.Green, message);
		}

		/// <summary>
		/// Writes a message to the console without leaving the current line.
		/// </summary>
		/// <param name="message">The message to write.</param>
		public void Write(ConsoleColor color, string message)
		{
			Console.ForegroundColor = color;
			Console.Write(message);
			Console.ForegroundColor = ConsoleColor.White;
		}

		/// <summary>
		/// Writes a message to the console, leaving the current line afterwards.
		/// </summary>
		/// <param name="message">The message to write.</param>
		public void WriteLine(ConsoleColor color, string message)
		{
			Console.ForegroundColor = color;
			Console.WriteLine(message);
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}
