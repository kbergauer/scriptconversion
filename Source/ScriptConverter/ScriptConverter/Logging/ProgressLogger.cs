﻿using System;

namespace ScriptConverter.Logging
{
	/// <summary>
	/// Logger used for simulating a progress bar.
	/// </summary>
	public class ProgressLogger
	{
		/// <summary>
		/// The logger used for writing to the current output.
		/// </summary>
		private ILogger m_logger;

		/// <summary>
		/// The total progress displayable by the bar.
		/// </summary>
		private float m_totalProgress;

		/// <summary>
		/// The current progress displayed by the bar.
		/// </summary>
		private float m_currentProgress;

		/// <summary>
		/// The step size to advance the progress by using <see cref="Advance"/>.
		/// </summary>
		private float m_progressStep;

		/// <summary>
		/// The console cursor's horizontal starting position from which to start writing.
		/// </summary>
		private int m_horizontalPosition;

		/// <summary>
		/// The console cursor's vertical starting position from which to start writing.
		/// </summary>
		private int m_verticalPosition;

		/// <summary>
		/// Constructor caching necessary values.
		/// </summary>
		/// <param name="total">The total displayable progress of the bar used to calculate the <see cref="Advance"/> step size.</param>
		/// <param name="logger">The logger used for writing to the output.</param>
		public ProgressLogger(float total, ILogger logger)
		{
			m_logger = logger;

			m_totalProgress = total;
			m_currentProgress = 0.0f;
			m_progressStep = 1.0f / m_totalProgress;

			m_horizontalPosition = Console.CursorLeft;
			m_verticalPosition = Console.CursorTop;
		}

		/// <summary>
		/// Increases the current progress by the progress step size and updates the display.
		/// </summary>
		public void Advance()
		{
			m_currentProgress += m_progressStep;
			SetProgess((int)(m_currentProgress * 100.0f));
		}

		/// <summary>
		/// Sets the displayed progress of the bar.
		/// </summary>
		/// <param name="progress">The progress to display.</param>
		public void SetProgess(int progress)
		{
			progress = Math.Min(100, Math.Max(0, progress));

			Console.CursorLeft = m_horizontalPosition;
			Console.CursorTop = m_verticalPosition;

			string bar = "";
			for (int i = 0; i < 20; i++)
			{
				bar += i * 5 <= progress ? "#" : " ";
			}

			m_logger.Write(ConsoleColor.White, $"[{bar}] {progress.ToString()}%\n");
		}
	}
}
