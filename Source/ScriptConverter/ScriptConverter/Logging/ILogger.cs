﻿using System;

namespace ScriptConverter.Logging
{
	/// <summary>
	/// Interface for classes providing logging functionality.
	/// </summary>
	public interface ILogger
	{
		/// <summary>
		/// Logs a debug message which gets stripped out for release builds.
		/// </summary>
		/// <param name="message">The message to log.</param>
		void Debug(string message);

		/// <summary>
		/// Logs an info message.
		/// </summary>
		/// <param name="message">The message to log.</param>
		void Info(string message);

		/// <summary>
		/// Logs a warning message.
		/// </summary>
		/// <param name="message">The message to log.</param>
		void Warn(string message);

		/// <summary>
		/// Logs an error message.
		/// </summary>
		/// <param name="message">The message to log.</param>
		void Error(string message);

		/// <summary>
		/// Logs a success message.
		/// </summary>
		/// <param name="message">The message to log.</param>
		void Success(string message);

		/// <summary>
		/// Writes a message to the logger's output without leaving the current line.
		/// </summary>
		/// <param name="message">The message to write.</param>
		void Write(ConsoleColor color, string message);

		/// <summary>
		/// Writes a message to the logger's output, leaving the current line afterwards.
		/// </summary>
		/// <param name="message">The message to write.</param>
		void WriteLine(ConsoleColor color, string message);
	}
}
